class Page < ActiveRecord::Base
	extend FriendlyId
  	friendly_id :name, use: :slugged
  	has_many :slides, -> { order 'position asc' }
  	has_attached_file :icon,
    :styles => {
      :thumb => "100x100#",
      :small  => "150x150>",
      :medium => "200x200" }
	  validates_attachment_content_type :icon, :content_type => /\Aimage\/.*\Z/
	  # add a delete_<asset_name> method: 
	  attr_accessor :delete_icon
	  before_validation { self.icon.clear if self.delete_icon == '1' }

	def color_enum
	    ['red','green','yellow','blue']
	end

	scope :published, -> { where(published: true).order('position ASC') }
end
