class Video < ActiveRecord::Base
  belongs_to :slide
  has_attached_file :m4v
  has_attached_file :mp4
  has_attached_file :ogv
  has_attached_file :webm
  has_attached_file :poster,
    :styles => {
      :thumb => "100x100#",
      :small  => "150x150>",
      :medium => "200x200" }
	  validates_attachment_content_type :poster, :content_type => /\Aimage\/.*\Z/
	  # add a delete_<asset_name> method: 
	  attr_accessor :delete_poster
	  before_validation { self.poster.clear if self.delete_poster == '1' }
end
