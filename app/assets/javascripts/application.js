// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require bootstrap.min
//= require jquery_ujs
//= require jquery_mousewheel


goog_snippet_vars = function() {
    var w = window;
      w.google_conversion_id = 925168686;
    w.google_conversion_label = "ZonbCOOJhmoQruiTuQM";
    w.google_conversion_color = "ffffff";
    
    w.google_remarketing_only = false;
  }
  
  // DO NOT CHANGE THE CODE BELOW.
goog_report_conversion = function(url) {
    goog_snippet_vars();
    window.google_conversion_format = "3";
    var opt = new Object();
    opt.onload_callback = function() {
    if (typeof(url) != 'undefined') {
      window.location = url;
    }
  }
  var conv_handler = window['google_trackConversion'];
  if (typeof(conv_handler) == 'function') {
    conv_handler(opt);
    }

}
$(document).ready(function () {

    $('[ data-toggle-menu]').on('click', function () {
        var btn = $(this),
            aside = $('aside'),
            isActive = btn.hasClass('active');
      btn[isActive ? 'removeClass' : 'addClass']('active');
      aside[isActive ? 'removeClass' : 'addClass']('active');
    });

    if ($('.slider').length > 0) slider.init();

    $('.form-control').on('keydown', function () {
        var value = $(this).val();
        if(value.length > 0){
            if($(this).parent().find('.clear-input').length == 0)
                $(this).parent().append('<div class="clear-input"></div>');
        } else {
            $(this).parent().find('.clear-input').remove();
        }
    });

    $(document).on('click', '.clear-input', function () {
        $(this).siblings('input').val('');
        $(this).remove();
    });

    video();

    $(document).on('submit', '.contact-form', function(e){
       e.preventDefault();

       if(CheckFormValidity($(this))){
           var btn = $(this).find('button'),
               data = $(this).serialize();
            $.ajax({
               type: "GET",
               url: "/contacts/new/",
               data: data,
               success: function(data) {
                   if(data == true){
                    //$('#thanks').modal('show');
                       goog_report_conversion('http://www.insightfulhr.com/contacts/');
                   } else{
                   }
              }  
           });
       }
   });

    $(document).on('click', '[data-open-submenu]', function(e){
        e.preventDefault();
        var link = $(this),
            li = link.parent(),
            index = link.index(),
            menu = $('.pages-menu'),
            submenu = link.next(),
            isOpen = submenu.hasClass('open');
 
        menu.find('.open').removeClass('open');
        li.siblings()[isOpen ? 'removeClass' : 'addClass']('hide-item'); 
        li[isOpen ? 'removeClass' : 'addClass']('opened'); 
        submenu[isOpen ? 'removeClass' : 'addClass']('open'); 




    });

});


function CheckEmail(str) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(str);
}

function CheckFormValidity(selector) {
    var result = true,
        input;

    $(selector).find('input:required,textarea:required').each(function () {
        input = $(this);
        if (
            input.val().length == 0
            || (input.attr('type') == 'email' && !CheckEmail(input.val()))
        ) {
            input.focus();
            result = false;
            return false;
        }
    });
    return result;
}
function video() {
    if($('#bgvid').length > 0) {
        var vid = document.getElementById("bgvid"),
            pauseButton = document.getElementById("vidpause");

        function vidFade() {
            vid.classList.add("stopfade");
            pauseButton.classList.add("stop");
            $('.item.active').removeClass('play-video');
        }

        vid.addEventListener('ended', function () {
            vid.pause();
            vidFade();
        });

        $('.close-video').on('click', function () {
            vid.pause();
            vidFade();
        });

        pauseButton.addEventListener("click", function () {
            vid.classList.toggle("stopfade");
            pauseButton.classList.toggle("stop");

            var wrapper = $('.item.active'),
                isPlay = wrapper.hasClass('play-video');

            wrapper[!isPlay ? 'addClass' : 'removeClass']('play-video');

            if (vid.paused)
                vid.play();
            else
                vid.pause();
        });
    }
}


var isFirstLoad = true;
var sliderInner = {

    options: {},


    setOptions: function () {
        this.options.activeItem = '.item.active';
        this.options.$slider = $(this.options.activeItem + ' .slider-inner');
        this.options.$container = $(this.options.activeItem + ' .slider-wrapper');
        this.options.$items = $(this.options.activeItem + ' .slider-inner').find('.slide');
        this.options.count = this.options.$items.length;
        this.options.width = this.options.$slider.width();
        this.options.paginationClass = 'inner-slider-navigation';
        this.options.pagenationStyledClass = 'slider-navigation';
    },

    init: function () {
        this.setOptions();
        if(this.options.count > 0) {
            this.options.$slider.width(this.options.width);
            this.options.$container.width(this.options.width * this.options.count);
            this.options.$container.css({display: 'flex', marginLeft: 0});

            this.navigation();


            this.bind();
        }
    },

    bind: function () {
        if(isFirstLoad) {

            $(document).on('click', this.options.activeItem + ' .' + this.options.paginationClass + ' li', function (e) {
                if ($(this).hasClass('active')) return false;

                var li = $(this),
                    index = li.index(),
                    isFirst = index == 0,
                    isLast = index == sliderInner.options.count - 1;

                li.addClass('active').siblings('li').removeClass('active');

                sliderInner.options.$container.css({marginLeft: '-' + index * sliderInner.options.width + 'px'});

                $(sliderInner.options.activeItem + ' .prev')[isFirst ? 'removeClass' : 'addClass']('active');
                $(sliderInner.options.activeItem + ' .next')[isLast ? 'removeClass' : 'addClass']('active');

            });



            $(document).on('click', '.next', function () {
                sliderInner.next();
            });

            $(document).on('click', '.prev', function () {
                sliderInner.prev();
            });

            isFirstLoad = false;
        }
    },

    next: function () {
        $(this.options.activeItem + ' .' + this.options.paginationClass + ' li.active').next('li').click();
    },

    prev: function () {
        $(this.options.activeItem + ' .' + this.options.paginationClass + ' li.active').prev('li').click();
    },


    navigation: function () {

        if(this.options.count > 1) {

            $('.' + this.options.paginationClass).remove();
            var navigation = document.createElement('ul');
            navigation.className = this.options.pagenationStyledClass + ' ' + this.options.paginationClass;

            for (var i = 0; i < this.options.count; i++) {
                var li = document.createElement('li');
                navigation.appendChild(li);
            }

            this.options.$slider.append(navigation);
            this.options.$slider.find('.' + this.options.paginationClass +' li').eq(0).addClass('active');


            this.initButtons();
        }

    },
    initButtons: function () {

        $('.prev').remove();
        $('.next').remove();
        var next = document.createElement('div'),
            prev = document.createElement('div');

        next.className = 'next active';
        prev.className = 'prev';

        this.options.$slider.append(next);
        this.options.$slider.append(prev);
    },
}


var slider = {

	options: {},

    setOptions: function () {
        this.options.$slider = $('.slider');
        this.options.$items = this.options.$slider.find('.item');
        this.options.count = this.options.$items.length;
        this.options.paginationClass = 'main-slider-navigation';
        this.options.pagenationStyledClass = 'slider-navigation';
        this.options.activeSlide = false;

        if(this.options.$slider.data('active-slide') != undefined ){
            this.options.activeSlide = $(this.options.$slider.data('active-slide')).index();
        }


        var wrap = document.createElement('div');
            wrap.className = 'wrapper-navigation';
        this.options.$slider.append(wrap);
    },

    init: function () {

        this.setOptions();

        this.navigation();
        this.options.$items.eq(0).addClass('active');

        if(this.options.activeSlide && this.options.activeSlide != 0){
            $('.' + slider.options.paginationClass + ' li').eq(this.options.activeSlide).click();
        }
        sliderInner.init();





    },

    next: function () {
        $('.' + slider.options.paginationClass +' li.active').next('li').click();
    },

    prev: function () {
        $('.' + slider.options.paginationClass +' li.active').prev('li').click();
    },

    navigation: function () {

        if(this.options.count > 1) {
            var navigation = document.createElement('ul');

            navigation.className = slider.options.pagenationStyledClass + ' ' + slider.options.paginationClass;

            for (var i = 0; i < this.options.count; i++) {
                var li = document.createElement('li');
                navigation.appendChild(li);
            }

            $('.wrapper-navigation').append(navigation);
            $('.main-slider-navigation li').eq(0).addClass('active');



            $(document).on('click', '.' + slider.options.paginationClass + ' li', function (e) {
                if ($(this).hasClass('active')) return false;

                var li = $(this),
                    index = li.index(),
                    isFirst = index == 0,
                    isLast = index == slider.options.count - 1;

                li.addClass('active').siblings('li').removeClass('active');
                slider.options.$items.eq(index).addClass('active').siblings('.item').removeClass('active').css({top: '100%'});
                slider.options.$items.eq(index).prev('.item').css({top: '-100%'});

                $('.up')[isFirst ? 'removeClass' : 'addClass']('active');
                $('.down')[isLast ? 'removeClass' : 'addClass']('active');

                sliderInner.init();
            });

            this.initButtons();
            this.initKey();
        }

    },
    initButtons: function () {
        var down = document.createElement('div'),
            up = document.createElement('div');

        down.className = 'down active';
        up.className = 'up';
 

        var icon = document.createElement('i');
        icon.className = 'icon-arrow';
        down.appendChild(icon);
        $('.wrapper-navigation').append(down);

        var icon = document.createElement('i');
        icon.className = 'icon-arrow';
        up.appendChild(icon);
        $('.wrapper-navigation').append(up);

        $('.down').on('click', function () {
            slider.next();
        });

        $('.up').on('click', function () {
            slider.prev();
        });

    },
    initKey: function () {
        $('html').on('keydown', function (e) {
            switch (e.keyCode){
                case 40:
                    slider.next();
                    break;
                case 38:
                    slider.prev();
                    break;
            }
        })
    }
};


var lastAnimation = 0;
var animationTime = 500; // in ms
var quietPeriod = 600; // in ms, time after animation to ignore mousescroll

function scrollThis(event, delta, deltaX, deltaY) {
    var timeNow = new Date().getTime();


    deltaOfInterest = deltaY;
    
    if (deltaOfInterest == 0) {
        // Uncomment if you want to use deltaX
        // event.preventDefault();
        return;
    }
    
    // Cancel scroll if currently animating or within quiet period
    if(timeNow - lastAnimation < quietPeriod + animationTime) {
        event.preventDefault();
        return;
    }

    if (deltaOfInterest < 0) {
       lastAnimation = timeNow;
        slider.next();
            
    } else {
        lastAnimation = timeNow;
        slider.prev();
    }
}

var map;
function initMap() {
    var customMapType = new google.maps.StyledMapType([
        {
            "featureType": "road.highway",
            "elementType": "geometry",
            "stylers": [
                { "saturation": -100 },
                { "lightness": -8 },
                { "gamma": 1.18 }
            ]
        }, {
            "featureType": "road.arterial",
            "elementType": "geometry",
            "stylers": [
                { "saturation": -100 },
                { "gamma": 1 },
                { "lightness": -24 }
            ]
        }, {
            "featureType": "poi",
            "elementType": "geometry",
            "stylers": [
                { "saturation": -100 }
            ]
        }, {
            "featureType": "administrative",
            "stylers": [
                { "saturation": -100 }
            ]
        }, {
            "featureType": "transit",
            "stylers": [
                { "saturation": -100 }
            ]
        }, {
            "featureType": "water",
            "elementType": "geometry.fill",
            "stylers": [
                { "color": "#cad2d4" }
            ]
        }, {
            "featureType": "road",
            "stylers": [
                { "saturation": -100 }
            ]
        }, {
            "featureType": "administrative",
            "stylers": [
                { "saturation": -100 }
            ]
        }, {
            "featureType": "landscape",
            "stylers": [
                { "saturation": -100 }
            ]
        }, {
            "featureType": "poi",
            "stylers": [
                { "saturation": -100 }
            ]
        }, {}
    ], {
        name: 'Custom Style'
    });
    var customMapTypeId = 'custom_style';

    var map = new google.maps.Map(document.getElementById('map'), {
        zoom:14,
        scrollwheel: false,
        center: {lat: 51.372036, lng: -0.460747},  // Brooklyn.
        mapTypeControlOptions: {
            mapTypeIds: [google.maps.MapTypeId.ROADMAP, customMapTypeId]
        }
    });

    map.mapTypes.set(customMapTypeId, customMapType);
    map.setMapTypeId(customMapTypeId);

    var image = '/assets/label.png';
    var beachMarker = new google.maps.Marker({
        position: {lat:  51.372036, lng: -0.460747},
        map: map,
        icon: image
    });
}



$('html').mousewheel(scrollThis);
