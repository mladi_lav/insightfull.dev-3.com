gapi.analytics.ready ->

  ###*
  # Authorize the user immediately if the user has already granted access.
  # If no access has been created, render an authorize button inside the
  # element with the ID "embed-api-auth-container".
  ###

  ###*
  # Draw the a chart.js line chart with data from the specified view that
  # overlays session data for the current week over session data for the
  # previous week.
  ###

  renderWeekOverWeekChart = (ids) ->
    # Adjust `now` to experiment with different days, for testing only...
    now = moment()
    # .subtract(3, 'day');
    thisWeek = query(
      'ids': ids
      'dimensions': 'ga:date,ga:nthDay'
      'metrics': 'ga:sessions'
      'start-date': moment(now).subtract(1, 'day').day(0).format('YYYY-MM-DD')
      'end-date': moment(now).format('YYYY-MM-DD'))
    lastWeek = query(
      'ids': ids
      'dimensions': 'ga:date,ga:nthDay'
      'metrics': 'ga:sessions'
      'start-date': moment(now).subtract(1, 'day').day(0).subtract(1, 'week').format('YYYY-MM-DD')
      'end-date': moment(now).subtract(1, 'day').day(6).subtract(1, 'week').format('YYYY-MM-DD'))
    Promise.all([
      thisWeek
      lastWeek
    ]).then (results) ->
      data1 = results[0].rows.map((row) ->
        +row[2]
      )
      data2 = results[1].rows.map((row) ->
        +row[2]
      )
      labels = results[1].rows.map((row) ->
        +row[0]
      )
      labels = labels.map((label) ->
        moment(label, 'YYYYMMDD').format 'ddd'
      )
      data = 
        labels: labels
        datasets: [
          {
            label: 'Last Week'
            fillColor: 'rgba(220,220,220,0.5)'
            strokeColor: 'rgba(220,220,220,1)'
            pointColor: 'rgba(220,220,220,1)'
            pointStrokeColor: '#fff'
            data: data2
          }
          {
            label: 'This Week'
            fillColor: 'rgba(151,187,205,0.5)'
            strokeColor: 'rgba(151,187,205,1)'
            pointColor: 'rgba(151,187,205,1)'
            pointStrokeColor: '#fff'
            data: data1
          }
        ]
      new Chart(makeCanvas('chart-1-container')).Line data
      generateLegend 'legend-1-container', data.datasets
      return
    return

  ###*
  # Draw the a chart.js bar chart with data from the specified view that
  # overlays session data for the current year over session data for the
  # previous year, grouped by month.
  ###

  renderYearOverYearChart = (ids) ->
    # Adjust `now` to experiment with different days, for testing only...
    now = moment()
    # .subtract(3, 'day');
    thisYear = query(
      'ids': ids
      'dimensions': 'ga:month,ga:nthMonth'
      'metrics': 'ga:users'
      'start-date': moment(now).date(1).month(0).format('YYYY-MM-DD')
      'end-date': moment(now).format('YYYY-MM-DD'))
    lastYear = query(
      'ids': ids
      'dimensions': 'ga:month,ga:nthMonth'
      'metrics': 'ga:users'
      'start-date': moment(now).subtract(1, 'year').date(1).month(0).format('YYYY-MM-DD')
      'end-date': moment(now).date(1).month(0).subtract(1, 'day').format('YYYY-MM-DD'))
    Promise.all([
      thisYear
      lastYear
    ]).then((results) ->
      data1 = results[0].rows.map((row) ->
        +row[2]
      )
      data2 = results[1].rows.map((row) ->
        +row[2]
      )
      labels = [
        'Jan'
        'Feb'
        'Mar'
        'Apr'
        'May'
        'Jun'
        'Jul'
        'Aug'
        'Sep'
        'Oct'
        'Nov'
        'Dec'
      ]
      # Ensure the data arrays are at least as long as the labels array.
      # Chart.js bar charts dont (yet) accept sparse datasets.
      i = 0
      len = labels.length
      while i < len
        if data1[i] == undefined
          data1[i] = null
        if data2[i] == undefined
          data2[i] = null
        i++
      data = 
        labels: labels
        datasets: [
          {
            label: 'Last Year'
            fillColor: 'rgba(220,220,220,0.5)'
            strokeColor: 'rgba(220,220,220,1)'
            data: data2
          }
          {
            label: 'This Year'
            fillColor: 'rgba(151,187,205,0.5)'
            strokeColor: 'rgba(151,187,205,1)'
            data: data1
          }
        ]
      new Chart(makeCanvas('chart-2-container')).Bar data
      generateLegend 'legend-2-container', data.datasets
      return
    ).catch (err) ->
      console.error err.stack
      return
    return

  ###*
  # Draw the a chart.js doughnut chart with data from the specified view that
  # show the top 5 browsers over the past seven days.
  ###

  renderTopBrowsersChart = (ids) ->
    query(
      'ids': ids
      'dimensions': 'ga:browser'
      'metrics': 'ga:pageviews'
      'sort': '-ga:pageviews'
      'max-results': 5).then (response) ->
      data = []
      colors = [
        '#4D5360'
        '#949FB1'
        '#D4CCC5'
        '#E2EAE9'
        '#F7464A'
      ]
      response.rows.forEach (row, i) ->
        data.push
          value: +row[1]
          color: colors[i]
          label: row[0]
        return
      new Chart(makeCanvas('chart-3-container')).Doughnut data
      generateLegend 'legend-3-container', data
      return
    return

  ###*
  # Draw the a chart.js doughnut chart with data from the specified view that
  # compares sessions from mobile, desktop, and tablet over the past seven
  # days.
  ###

  renderTopCountriesChart = (ids) ->
    query(
      'ids': ids
      'dimensions': 'ga:country'
      'metrics': 'ga:sessions'
      'sort': '-ga:sessions'
      'max-results': 5).then (response) ->
      data = []
      colors = [
        '#4D5360'
        '#949FB1'
        '#D4CCC5'
        '#E2EAE9'
        '#F7464A'
      ]
      response.rows.forEach (row, i) ->
        data.push
          label: row[0]
          value: +row[1]
          color: colors[i]
        return
      new Chart(makeCanvas('chart-4-container')).Doughnut data
      generateLegend 'legend-4-container', data
      return
    return

  ###*
  # Extend the Embed APIs `gapi.analytics.report.Data` component to
  # return a promise the is fulfilled with the value returned by the API.
  # @param {Object} params The request parameters.
  # @return {Promise} A promise.
  ###

  query = (params) ->
    new Promise((resolve, reject) ->
      data = new (gapi.analytics.report.Data)(query: params)
      data.once('success', (response) ->
        resolve response
        return
      ).once('error', (response) ->
        reject response
        return
      ).execute()
      return
)

  ###*
  # Create a new canvas inside the specified element. Set it to be the width
  # and height of its container.
  # @param {string} id The id attribute of the element to host the canvas.
  # @return {RenderingContext} The 2D canvas context.
  ###

  makeCanvas = (id) ->
    container = document.getElementById(id)
    canvas = document.createElement('canvas')
    ctx = canvas.getContext('2d')
    container.innerHTML = ''
    canvas.width = container.offsetWidth
    canvas.height = container.offsetHeight
    container.appendChild canvas
    ctx

  ###*
  # Create a visual legend inside the specified element based off of a
  # Chart.js dataset.
  # @param {string} id The id attribute of the element to host the legend.
  # @param {Array.<Object>} items A list of labels and colors for the legend.
  ###

  generateLegend = (id, items) ->
    legend = document.getElementById(id)
    legend.innerHTML = items.map((item) ->
      color = item.color or item.fillColor
      label = item.label
      '<li><i style="background:' + color + '"></i>' + label + '</li>'
    ).join('')
    return

  gapi.analytics.auth.authorize
    container: 'embed-api-auth-container'
    clientid: '457884467434-2l74pb7glmvthn1ro241rtd08pb6q6uk.apps.googleusercontent.com'

  ###*
  # Create a new ActiveUsers instance to be rendered inside of an
  # element with the id "active-users-container" and poll for changes every
  # five seconds.
  ###

  activeUsers = new (gapi.analytics.ext.ActiveUsers)(
    container: 'active-users-container'
    pollingInterval: 5)

  ###*
  # Add CSS animation to visually show the when users come and go.
  ###

  activeUsers.once 'success', ->
    element = @container.firstChild
    timeout = undefined
    @on 'change', (data) ->
      `var element`
      element = @container.firstChild
      animationClass = if data.delta > 0 then 'is-increasing' else 'is-decreasing'
      element.className += ' ' + animationClass
      clearTimeout timeout
      timeout = setTimeout((->
        element.className = element.className.replace(RegExp(' is-(increasing|decreasing)', 'g'), '')
        return
      ), 3000)
      return
    return

  ###*
  # Create a new ViewSelector2 instance to be rendered inside of an
  # element with the id "view-selector-container".
  ###

  viewSelector = new (gapi.analytics.ext.ViewSelector2)(container: 'view-selector-container').execute()

  ###*
  # Update the activeUsers component, the Chartjs charts, and the dashboard
  # title whenever the user changes the view.
  ###

  viewSelector.on 'viewChange', (data) ->
    title = document.getElementById('view-name')
    title.innerHTML = data.property.name + ' (' + data.view.name + ')'
    # Start tracking active users for this view.
    activeUsers.set(data).execute()
    # Render all the of charts for this view.
    renderWeekOverWeekChart data.ids
    renderYearOverYearChart data.ids
    renderTopBrowsersChart data.ids
    renderTopCountriesChart data.ids
    return
  # Set some global Chart.js defaults.
  Chart.defaults.global.animationSteps = 60
  Chart.defaults.global.animationEasing = 'easeInOutQuart'
  Chart.defaults.global.responsive = true
  Chart.defaults.global.maintainAspectRatio = false
  return

# ---
# generated by js2coffee 2.2.0