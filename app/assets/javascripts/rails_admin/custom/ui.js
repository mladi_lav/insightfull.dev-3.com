+function ($) {

  $(function(){

      $(document).on('click', '[ui-toggle-class]', function (e) {
        e.preventDefault();
        var $this = $(e.target);
        $this.attr('ui-toggle-class') || ($this = $this.closest('[ui-toggle-class]'));
        
		var classes = $this.attr('ui-toggle-class').split(','),
			targets = ($this.attr('target') && $this.attr('target').split(',')) || Array($this),
			key = 0;
		$.each(classes, function( index, value ) {
			var target = targets[(targets.length && key)];
			$( target ).toggleClass(classes[index]);
			key ++;
		});
		$this.toggleClass('active');

      });
  });
}(jQuery);

+function ($) {

  $(function(){

      // nav
      $(document).on('click', '[ui-nav] a', function (e) {
        var $this = $(e.target), $active;
        $this.is('a') || ($this = $this.closest('a'));
        
        $active = $this.parent().siblings( ".active" );
        $active && $active.toggleClass('active').find('> ul:visible').slideUp(200);
        
        ($this.parent().hasClass('active') && $this.next().slideUp(200)) || $this.next().slideDown(200);
        $this.parent().toggleClass('active');
        
        $this.next().is('ul') && e.preventDefault();
      });

  });
}(jQuery);


/**
 * 0.1.0
 * Deferred load js/css file, used for ui-jq.js and Lazy Loading.
 * 
 * @ flatfull.com All Rights Reserved.
 * Author url: http://themeforest.net/user/flatfull
 */
var uiLoad = uiLoad || {};

(function($, $document, uiLoad) {
  "use strict";

    var loaded = [],
    promise = false,
    deferred = $.Deferred();

    /**
     * Chain loads the given sources
     * @param srcs array, script or css
     * @returns {*} Promise that will be resolved once the sources has been loaded.
     */
    uiLoad.load = function (srcs) {
      srcs = $.isArray(srcs) ? srcs : srcs.split(/\s+/);
      if(!promise){
        promise = deferred.promise();
      }

      $.each(srcs, function(index, src) {
        promise = promise.then( function(){
          return src.indexOf('.css') >=0 ? loadCSS(src) : loadScript(src);
        } );
      });
      deferred.resolve();
      return promise;
    };

    /**
     * Dynamically loads the given script
     * @param src The url of the script to load dynamically
     * @returns {*} Promise that will be resolved once the script has been loaded.
     */
    var loadScript = function (src) {
      if(loaded[src]) return loaded[src].promise();

      var deferred = $.Deferred();
      var script = $document.createElement('script');
      script.src = src;
      script.onload = function (e) {
        deferred.resolve(e);
      };
      script.onerror = function (e) {
        deferred.reject(e);
      };
      $document.body.appendChild(script);
      loaded[src] = deferred;

      return deferred.promise();
    };

    /**
     * Dynamically loads the given CSS file
     * @param href The url of the CSS to load dynamically
     * @returns {*} Promise that will be resolved once the CSS file has been loaded.
     */
    var loadCSS = function (href) {
      if(loaded[href]) return loaded[href].promise();

      var deferred = $.Deferred();
      var style = $document.createElement('link');
      style.rel = 'stylesheet';
      style.type = 'text/css';
      style.href = href;
      style.onload = function (e) {
        deferred.resolve(e);
      };
      style.onerror = function (e) {
        deferred.reject(e);
      };
      $document.head.appendChild(style);
      loaded[href] = deferred;

      return deferred.promise();
    }

})(jQuery, document, uiLoad);


+function ($) {

  $(function(){

      $("[ui-jq]").each(function(){
        var self = $(this);
        var options = eval('[' + self.attr('ui-options') + ']');

        if ($.isPlainObject(options[0])) {
          options[0] = $.extend({}, options[0]);
        }

        uiLoad.load(jp_config[self.attr('ui-jq')]).then( function(){          
          self[self.attr('ui-jq')].apply(self, options);
        });
      });

  });
}(jQuery);

+function ($) {

  $(function(){

    // Checks for ie
    var isIE = !!navigator.userAgent.match(/MSIE/i) || !!navigator.userAgent.match(/Trident.*rv:11\./);
    isIE && $('html').addClass('ie');

  // Checks for iOs, Android, Blackberry, Opera Mini, and Windows mobile devices
  var ua = window['navigator']['userAgent'] || window['navigator']['vendor'] || window['opera'];
  (/iPhone|iPod|iPad|Silk|Android|BlackBerry|Opera Mini|IEMobile/).test(ua) && $('html').addClass('smart');

  });
}(jQuery);

