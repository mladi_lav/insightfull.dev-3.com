class UserMailer < ApplicationMailer
	default from: 'notifications@example.com'


  def callback_email(contact)
    @contact = contact
    mail(to: 'sharnel.hogg@insightfulhr.com', subject: 'New message')
  end
end
