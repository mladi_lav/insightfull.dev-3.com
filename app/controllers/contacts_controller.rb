class ContactsController < ApplicationController
	def index
		@pages = Page.published
    	@socials = Social.all.order('position ASC')
		render layout: 'main'

	end
	def new
	  	@contact = Contact.new(contact_params)
	  	@result = false
	  	if @contact.save
	   		UserMailer.callback_email(@contact).deliver_now
	   		@result = true
	   	end
	   	 
	end

  def create
   
  end
	private
	    def contact_params
	      params.require(:contact).permit(:name, :email, :phone, :comment, :subject)
	    end

end
