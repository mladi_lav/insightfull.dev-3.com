class PagesController < ApplicationController

	before_action :set_page, only: [:show]

	def index

    @pages = Page.published
    @socials = Social.all.order('position ASC')
    @clients = Client.show
		@partners = Partner.all.order('position ASC')
		render layout: 'main'
  end

  def show
    @contact = Contact.new
    @pages = Page.published
    @socials = Social.all.order('position ASC')
      
		render layout: 'pages'
  end

  def set_page
    @page = Page.friendly.find(params[:id])
  end

end
