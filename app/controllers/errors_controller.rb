class ErrorsController < ApplicationController
  def error404
  	@pages = Page.published
    @socials = Social.all.order('position ASC')
    render status: :not_found, layout: 'main'
  end
end