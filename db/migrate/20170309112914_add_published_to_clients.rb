class AddPublishedToClients < ActiveRecord::Migration
  def change
    add_column :clients, :published, :boolean
  end
end
