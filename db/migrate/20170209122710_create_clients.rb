class CreateClients < ActiveRecord::Migration
  def change
    create_table :clients do |t|
      t.string :name
      t.string :link
      t.integer :position
      t.references :slide, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
