class AddAttachmentPosterToVideos < ActiveRecord::Migration
  def self.up
    change_table :videos do |t|
      t.attachment :poster
    end
  end

  def self.down
    remove_attachment :videos, :poster
  end
end
