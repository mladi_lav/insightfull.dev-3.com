class AddPublishedToTestimonials < ActiveRecord::Migration
  def change
    add_column :testimonials, :published, :boolean
  end
end
