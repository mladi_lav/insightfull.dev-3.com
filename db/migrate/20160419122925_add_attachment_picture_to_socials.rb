class AddAttachmentPictureToSocials < ActiveRecord::Migration
  def self.up
    change_table :socials do |t|
      t.attachment :picture
    end
  end

  def self.down
    remove_attachment :socials, :picture
  end
end
