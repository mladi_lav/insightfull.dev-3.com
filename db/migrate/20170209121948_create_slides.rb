class CreateSlides < ActiveRecord::Migration
  def change
    create_table :slides do |t|
      t.string :name
      t.text :text
      t.integer :position
      t.boolean :form
      t.references :page, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
