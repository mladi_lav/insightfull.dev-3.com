class AddAttachmentOgvToVideos < ActiveRecord::Migration
  def self.up
    change_table :videos do |t|
      t.attachment :ogv
    end
  end

  def self.down
    remove_attachment :videos, :ogv
  end
end
