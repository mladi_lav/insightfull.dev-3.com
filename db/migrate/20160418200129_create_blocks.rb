class CreateBlocks < ActiveRecord::Migration
  def change
    create_table :blocks do |t|
      t.string :name
      t.text :text
      t.string :link
      t.string :template

      t.timestamps null: false
    end
  end
end
