class AddAttachmentIconToPages < ActiveRecord::Migration
  def self.up
    change_table :pages do |t|
      t.attachment :icon
    end
  end

  def self.down
    remove_attachment :pages, :icon
  end
end
