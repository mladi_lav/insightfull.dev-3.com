class AddContactToPages < ActiveRecord::Migration
  def change
    add_column :pages, :contact, :boolean
  end
end
