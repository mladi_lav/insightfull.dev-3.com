class AddAttachmentPictureToDownloads < ActiveRecord::Migration
  def self.up
    change_table :downloads do |t|
      t.attachment :picture
    end
  end

  def self.down
    remove_attachment :downloads, :picture
  end
end
