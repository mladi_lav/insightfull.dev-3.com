class CreateReviews < ActiveRecord::Migration
  def change
    create_table :reviews do |t|
      t.string :name
      t.string :color
      t.text :text

      t.timestamps null: false
    end
  end
end
