class AddAttachmentWebmToVideos < ActiveRecord::Migration
  def self.up
    change_table :videos do |t|
      t.attachment :webm
    end
  end

  def self.down
    remove_attachment :videos, :webm
  end
end
