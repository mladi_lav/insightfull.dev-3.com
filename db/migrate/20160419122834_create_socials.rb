class CreateSocials < ActiveRecord::Migration
  def change
    create_table :socials do |t|
      t.string :name
      t.string :link
      t.integer :position

      t.timestamps null: false
    end
  end
end
