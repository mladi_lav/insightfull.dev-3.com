class AddShowNameToSliders < ActiveRecord::Migration
  def change
    add_column :sliders, :show_name, :boolean
  end
end
