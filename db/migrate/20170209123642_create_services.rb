class CreateServices < ActiveRecord::Migration
  def change
    create_table :services do |t|
      t.text :text
      t.integer :position
      t.references :slide, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
