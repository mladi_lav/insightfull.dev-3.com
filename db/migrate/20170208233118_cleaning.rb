class Cleaning < ActiveRecord::Migration
  def change
  	drop_table :addresses
  	drop_table :blocks
  	drop_table :downloads
  	drop_table :logos
  	drop_table :menus
  	drop_table :reviews
  	drop_table :sliders
  end
end
