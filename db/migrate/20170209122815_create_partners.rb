class CreatePartners < ActiveRecord::Migration
  def change
    create_table :partners do |t|
      t.string :name
      t.string :link
      t.integer :position
      t.references :slide, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
