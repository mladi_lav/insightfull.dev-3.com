class CreateDownloads < ActiveRecord::Migration
  def change
    create_table :downloads do |t|
      t.string :name
      t.text :text
      t.integer :position

      t.timestamps null: false
    end
  end
end
