class AddAttachmentPictureToClients < ActiveRecord::Migration
  def self.up
    change_table :clients do |t|
      t.attachment :picture
    end
  end

  def self.down
    remove_attachment :clients, :picture
  end
end
