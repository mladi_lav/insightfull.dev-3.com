class CreateTestimonials < ActiveRecord::Migration
  def change
    create_table :testimonials do |t|
      t.string :name
      t.text :description
      t.text :author
      t.references :slide, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
