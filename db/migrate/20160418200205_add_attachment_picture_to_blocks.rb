class AddAttachmentPictureToBlocks < ActiveRecord::Migration
  def self.up
    change_table :blocks do |t|
      t.attachment :picture
    end
  end

  def self.down
    remove_attachment :blocks, :picture
  end
end
