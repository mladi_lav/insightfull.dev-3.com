class AddAttachmentM4vToVideos < ActiveRecord::Migration
  def self.up
    change_table :videos do |t|
      t.attachment :m4v
    end
  end

  def self.down
    remove_attachment :videos, :m4v
  end
end
