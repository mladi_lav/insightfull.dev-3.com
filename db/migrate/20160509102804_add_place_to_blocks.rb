class AddPlaceToBlocks < ActiveRecord::Migration
  def change
    add_column :blocks, :place, :string
  end
end
