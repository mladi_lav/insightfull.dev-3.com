# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170309112926) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "ckeditor_assets", force: :cascade do |t|
    t.string   "data_file_name",               null: false
    t.string   "data_content_type"
    t.integer  "data_file_size"
    t.string   "data_fingerprint"
    t.integer  "assetable_id"
    t.string   "assetable_type",    limit: 30
    t.string   "type",              limit: 30
    t.integer  "width"
    t.integer  "height"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
  end

  add_index "ckeditor_assets", ["assetable_type", "assetable_id"], name: "idx_ckeditor_assetable", using: :btree
  add_index "ckeditor_assets", ["assetable_type", "type", "assetable_id"], name: "idx_ckeditor_assetable_type", using: :btree

  create_table "clients", force: :cascade do |t|
    t.string   "name"
    t.string   "link"
    t.integer  "position"
    t.integer  "slide_id"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.string   "picture_file_name"
    t.string   "picture_content_type"
    t.integer  "picture_file_size"
    t.datetime "picture_updated_at"
    t.boolean  "show"
    t.boolean  "published"
  end

  add_index "clients", ["slide_id"], name: "index_clients_on_slide_id", using: :btree

  create_table "contacts", force: :cascade do |t|
    t.string   "name"
    t.string   "email"
    t.string   "phone"
    t.text     "comment"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "subject"
  end

  create_table "courses", force: :cascade do |t|
    t.string   "name"
    t.text     "text"
    t.integer  "position"
    t.integer  "slide_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "courses", ["slide_id"], name: "index_courses_on_slide_id", using: :btree

  create_table "friendly_id_slugs", force: :cascade do |t|
    t.string   "slug",                      null: false
    t.integer  "sluggable_id",              null: false
    t.string   "sluggable_type", limit: 50
    t.string   "scope"
    t.datetime "created_at"
  end

  add_index "friendly_id_slugs", ["slug", "sluggable_type", "scope"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope", unique: true, using: :btree
  add_index "friendly_id_slugs", ["slug", "sluggable_type"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type", using: :btree
  add_index "friendly_id_slugs", ["sluggable_id"], name: "index_friendly_id_slugs_on_sluggable_id", using: :btree
  add_index "friendly_id_slugs", ["sluggable_type"], name: "index_friendly_id_slugs_on_sluggable_type", using: :btree

  create_table "pages", force: :cascade do |t|
    t.string   "name"
    t.string   "color"
    t.string   "title"
    t.text     "description"
    t.string   "keywords"
    t.integer  "position"
    t.boolean  "published"
    t.string   "slug"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.string   "icon_file_name"
    t.string   "icon_content_type"
    t.integer  "icon_file_size"
    t.datetime "icon_updated_at"
    t.boolean  "contact"
  end

  add_index "pages", ["slug"], name: "index_pages_on_slug", unique: true, using: :btree

  create_table "partners", force: :cascade do |t|
    t.string   "name"
    t.string   "link"
    t.integer  "position"
    t.integer  "slide_id"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.string   "picture_file_name"
    t.string   "picture_content_type"
    t.integer  "picture_file_size"
    t.datetime "picture_updated_at"
  end

  add_index "partners", ["slide_id"], name: "index_partners_on_slide_id", using: :btree

  create_table "people", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.integer  "slide_id"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.string   "photo_file_name"
    t.string   "photo_content_type"
    t.integer  "photo_file_size"
    t.datetime "photo_updated_at"
    t.integer  "position"
  end

  add_index "people", ["slide_id"], name: "index_people_on_slide_id", using: :btree

  create_table "rails_admin_settings", force: :cascade do |t|
    t.boolean  "enabled",           default: true
    t.string   "kind",              default: "string", null: false
    t.string   "ns",                default: "main"
    t.string   "key",                                  null: false
    t.text     "raw"
    t.string   "label"
    t.string   "file_file_name"
    t.string   "file_content_type"
    t.integer  "file_file_size"
    t.datetime "file_updated_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "rails_admin_settings", ["key"], name: "index_rails_admin_settings_on_key", using: :btree
  add_index "rails_admin_settings", ["ns", "key"], name: "index_rails_admin_settings_on_ns_and_key", unique: true, using: :btree

  create_table "services", force: :cascade do |t|
    t.text     "text"
    t.integer  "position"
    t.integer  "slide_id"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.string   "picture_file_name"
    t.string   "picture_content_type"
    t.integer  "picture_file_size"
    t.datetime "picture_updated_at"
  end

  add_index "services", ["slide_id"], name: "index_services_on_slide_id", using: :btree

  create_table "slides", force: :cascade do |t|
    t.string   "name"
    t.text     "text"
    t.integer  "position"
    t.boolean  "form"
    t.integer  "page_id"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.string   "picture_file_name"
    t.string   "picture_content_type"
    t.integer  "picture_file_size"
    t.datetime "picture_updated_at"
  end

  add_index "slides", ["page_id"], name: "index_slides_on_page_id", using: :btree

  create_table "socials", force: :cascade do |t|
    t.string   "name"
    t.string   "link"
    t.integer  "position"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.string   "picture_file_name"
    t.string   "picture_content_type"
    t.integer  "picture_file_size"
    t.datetime "picture_updated_at"
  end

  create_table "testimonials", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.text     "author"
    t.integer  "slide_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.integer  "position"
    t.boolean  "published"
  end

  add_index "testimonials", ["slide_id"], name: "index_testimonials_on_slide_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  create_table "videos", force: :cascade do |t|
    t.string   "name"
    t.integer  "slide_id"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.string   "poster_file_name"
    t.string   "poster_content_type"
    t.integer  "poster_file_size"
    t.datetime "poster_updated_at"
    t.string   "m4v_file_name"
    t.string   "m4v_content_type"
    t.integer  "m4v_file_size"
    t.datetime "m4v_updated_at"
    t.string   "mp4_file_name"
    t.string   "mp4_content_type"
    t.integer  "mp4_file_size"
    t.datetime "mp4_updated_at"
    t.string   "ogv_file_name"
    t.string   "ogv_content_type"
    t.integer  "ogv_file_size"
    t.datetime "ogv_updated_at"
    t.string   "webm_file_name"
    t.string   "webm_content_type"
    t.integer  "webm_file_size"
    t.datetime "webm_updated_at"
  end

  add_index "videos", ["slide_id"], name: "index_videos_on_slide_id", using: :btree

  add_foreign_key "clients", "slides"
  add_foreign_key "courses", "slides"
  add_foreign_key "partners", "slides"
  add_foreign_key "people", "slides"
  add_foreign_key "services", "slides"
  add_foreign_key "slides", "pages"
  add_foreign_key "testimonials", "slides"
  add_foreign_key "videos", "slides"
end
