RailsAdmin.config do |config|

  ### Popular gems integration

  ## == Devise ==
   config.authenticate_with do
     warden.authenticate! scope: :user
   end
   config.current_user_method(&:current_user)

  ## == Cancan ==
  # config.authorize_with :cancan

  ## == Pundit ==
  # config.authorize_with :pundit

  ## == PaperTrail ==
  # config.audit_with :paper_trail, 'User', 'PaperTrail::Version' # PaperTrail >= 3.0.0

  ### More at https://github.com/sferik/rails_admin/wiki/Base-configuration

  config.excluded_models = ["Ckeditor::AttachmentFile", "Ckeditor::Picture", "Ckeditor::Asset", "RailsAdminSettings::Setting"]

  config.model Client do
    navigation_label 'Resourses'
    navigation_icon 'fa fa-folder-open'
    nestable_list true
    weight -9

    edit do
      fields :name, :slide, :picture do
        help 'Required field'
        required true 
      end
      fields :link, :published do
        help ''
      end

      field :show do
        help 'Show on the main page'
      end
    end

    exclude_fields :position
  end


  config.model Partner do
    navigation_label 'Resourses'
    navigation_icon 'fa fa-folder-open'
    nestable_list true
    weight -8
    exclude_fields :position
    edit do
      fields :name, :picture do
        help 'Required field'
        required true 
      end
      fields :link, :slide do
        help ''
      end
    end
  end

  config.model Course do
    navigation_label 'Resourses'
    navigation_icon 'fa fa-graduation-cap'
    nestable_list true
    weight -7
    exclude_fields :position
    edit do
      fields :name, :text, :slide do
        help 'Required field'
        required true 
      end
    end

  end



  config.model Video do
    navigation_label 'Resourses'
    navigation_icon 'fa fa-film'
    weight -5
    nestable_list true
    edit do
      fields :slide, :poster, :m4v, :mp4, :ogv, :webm do
        help 'Required field'
        required true 
      end
    end
  end


  config.model Person do
    navigation_label 'Resourses'
    navigation_icon 'fa fa-users'
    nestable_list true
    weight -4
    exclude_fields :position
    edit do
      fields :name, :description, :slide, :photo do
        help 'Required field'
        required true 
      end
    end

  end


  config.model Testimonial do
    navigation_label 'Resourses'
    navigation_icon 'fa fa-comments'
    nestable_list true
    weight -4
    exclude_fields :position
    edit do
      fields :name, :description, :author, :slide do
        help 'Required field'
        required true 
      end

      field :published do
        help ''
      end
    end

  end



  config.model Service do
    navigation_label 'Resourses'
    navigation_icon 'fa fa-cubes'
    nestable_list true
    weight -6
    edit do
      fields :slide, :picture, :text do
        help 'Required field'
        required true 
      end
    end
    exclude_fields :position
  end

  
  config.model Social do

    navigation_icon 'fa fa-share-alt'
    navigation_label 'Settings'
    edit do
      fields :name, :link, :picture do
        help 'Required field'
        required true 
      end
    end
    nestable_list true
  end 

  config.model Contact do
    
    navigation_label 'Settings'
    navigation_icon 'fa fa-envelope'
  end

  config.model User do
    navigation_label 'Settings'
    navigation_icon 'fa fa-user icon text-info-lter'
  end


  config.model Page do
    navigation_label 'Pages'
    navigation_icon 'fa fa-sticky-note'
    nestable_list true
    weight -10
    configure :icon do
      pretty_value do
        bindings[:view].tag(:img, { :src => value, :height => '150px' }) 
      end
    end

    edit do
      fields :name, :title, :icon, :color do
        help 'Required field'
        required true 
      end
      fields :published, :contact, :description, :keywords do
        help ''
      end
    end

    exclude_fields :position
  end



  config.model Slide do
    navigation_label 'Pages'
    navigation_icon 'fa fa-database'
    nestable_list true
    weight -10

    edit do
      fields :name, :picture do
        help 'Required field'
        required true 
      end
      field :text, :ck_editor
      fields :form, :page do 
        help ''
      end
    end

    exclude_fields :position


  end

  config.actions do

    dashboard                     # mandatory
    index                         # mandatory
    nestable
    new
    export
    bulk_delete
    show
    edit
    delete
    show_in_app

    ## With an audit adapter, you can add:
    # history_index
    # history_show
   
  end
end



